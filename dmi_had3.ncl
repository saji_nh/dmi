load "$SysE/lib/ncl/helper_libs.ncl"

function get_data(src,lat,lon,fnm)
begin
  opt = True
  ;opt@local_file = src+"_sst_"+fnm+".nc"
  time=(/"01Jan1950", "31Dec2017"/)

  ; syse_get_mon_anom is defined in the SysE library

  var = syse_get_mon_anom(src,"sst",lat,lon,time,opt)
  var1d = var(:,0,0)
  var1d=wgt_areaave(var,1.0,1.0,0)
  var1d = (/ dtrend(var1d, False) /)
  return(var1d)
end


src="hsst3"
fname = src+"_dmi"
; implement your own get_data -- all it does is to
; calculate indices based on latitude and longitude ranges
; here, we are getting indices of win, the western box of DMI,
;  eastern box of DMI (ein),
; nin = nino3 index
; n34 = nino3.4 index
; dmi = win-ein
; notice that linear trends are removed by the function get_data
; before returning the index

win= get_data( src,(/-10,10/),(/60,80/),"win")
ein= get_data( src,(/-10,0/),(/90,110/),"ein")
nin= get_data( src,(/-5,5/),(/210,270/),"nin")
n34= get_data( src,(/-5,5/),(/190,240/),"n34")
dmi = win
dmi = (/ win - ein /)
wstd = stddev(win)
estd = stddev(ein)
nstd = stddev(nin)
n34d = stddev(n34)
dstd = stddev(dmi)

pp(wstd)
pp(estd)
pp(dstd)
pp(nstd)
pp(n34d)
d1 = new((/dimsizes(dmi),1,1/),typeof(dmi))

; add_dimensions is defined in the SysE library

add_dimensions(d1,(/"time","lat","lon"/))
lat=0.0
lat@units="degrees_north"
lon=0.0
lon@units="degrees_east"
d1&lat=lat
d1&lon=lon
d1&time=dmi&time
w1=d1
e1=d1
n1=d1
n2=d1

d1=(/dmi/)
w1=(/win/)
e1=(/ein/)
n1=(/nin/)
n2=(/n34/)


; Write output as netcdf
; rm_file_if_present is defined in the SysE library
; fopen_write is defined in the SysE library

rm_file_if_present(fname+".nc")
fout=fopen_write(fname+".nc")

fout->dmi=d1
fout->win=w1
fout->ein=e1
fout->nin=n1
fout->n34=n2
