# DMI

How I construct the DMI index is based on the following article.

Saji, N. H., & Yamagata, T. (2003). Structure of SST and Surface Wind
Variability during Indian Ocean Dipole Mode Events: COADS Observations, Journal
of Climate, 16(16), 2735-2751.

## Notes 

I do not provide DMI data. However, I provide sample codes written in
NCAR Command Language (NCL) that outlines how to construct the DMI. 

However, the DMI constructed this way may contain zonal difference 
artefacts from two
unrelated physical processes that  affect the
western tropical Indian Ocean in a different manner from the 
eastern tropical Indian Ocean. Both produce artefacts of similar amplitude,
and it is recommended that both be removed. A sample code that outlines
how to remove these is also provided.

### What is SysE?

SysE is a library of NCL functions and procedures that I use often in
my work. You can find it at https://github.com/sajinh/SysE
In the sample codes, it is loaded by adding the following to the
top of the code.

`load "$SysE/lib/ncl/helper_libs.ncl"`

### Sample code to construct DMI

A sample code, `dmi_had3.ncl`, shows how to construct DMI from 
an SST dataset.
In this example, Hadley Center SST 
(https://www.metoffice.gov.uk/hadobs/hadsst3/)
is used. It is an insitu dataset that do not use interpolation procedures, but
rather accumulates available data into a pre-determined boxes.

Note that, DMI has a slightly different definition from Saji et al. 1999. Please consult Saji and Yamagata (2003) for the rationale to shift the western box
slightly eastward.

The code also calculates nin (Nino3) and n34 (Nino3.4) indices. These are used
in `bias_correct_dmi.ncl` to remove DMI artefacts introduced by the differential
effect of ENSO's lagged impact on the western and eastern Indian Ocean (see
Saji and Yamagata, 2003).


### Sample code to remove DMI artefacts

A sample code, `bias_correct_dmi.ncl`, removes the estimated DMI artefacts
resulting from secular trends, decadal fluctuations, and ENSO's lagged impact
on the tropical Indian Ocean.  


### Why does DMI vary from one dataset to another?

This is the wrong question. The right question to ask is why are there
different SST datasets, even for in-situ shipbased observations (e.g.
COADS, HadSST3).

In addition to the different datasets for the same ocean data, there are
SST datasets that use multiple methods to interpolate the available
data in space and time. Further, many investigators may correct
perceived biases in the data using different approaches. 

The factors mentioned above introduce considerable variation to DMI.
Fortunately, strong events are not significantly affected by such
artefacts. However, it introduces considerable uncertainty in identifying
moderately strong events. We suggest that when in doubt, you should
consider cross checking the state of DMI with other variables such
as equatorial zonal wind anomalies in the central Indian Ocean or
Outgoing Longwave Radiation anomalies, rainfall anomalies, Sea Surface Height
Anomalies etc. when they are available. It may however be advisable to
restrict the analysis using strong IOD events when possible.


